(function() {
  /* global angular */
  
  function podrobnostiLokacijeCtrl($routeParams, $location, $uibModal, edugeocachePodatki, avtentikacija) {
    var vm = this;
    vm.idLokacije = $routeParams.idLokacije;
    
    vm.jePrijavljen = avtentikacija.jePrijavljen();
    
    if (avtentikacija.jePrijavljen()) {
      var zeton = avtentikacija.vrniZeton();
      var koristnaVsebina = JSON.parse(avtentikacija.b64Utf8(zeton.split('.')[1]));
      //console.log(avtor + " " + koristnaVsebina.ime + " " + koristnaVsebina._id)
      vm.avtorMail = koristnaVsebina.elektronskiNaslov;
    }

    vm.jeAvtor = function(avtor) {
      if (avtentikacija.jePrijavljen()) {
        var zeton = avtentikacija.vrniZeton();
        var koristnaVsebina = JSON.parse(avtentikacija.b64Utf8(zeton.split('.')[1]));
        //console.log(avtor + " " + koristnaVsebina.ime + " " + koristnaVsebina._id)
        return avtor === koristnaVsebina.elektronskiNaslov;
      }
    }
    
    vm.prvotnaStran = $location.path();
    
    edugeocachePodatki.podrobnostiLokacijeZaId(vm.idLokacije).then(
      function success(odgovor) {
        vm.podatki = { lokacija: odgovor.data };
        vm.glavaStrani = {
          naslov: vm.podatki.lokacija.naziv
        };
      },
      function error(odgovor) {
        console.log(odgovor.e);
      }
    );
    
    vm.posodobiPodatke = function() {
      edugeocachePodatki.podrobnostiLokacijeZaId(vm.idLokacije).then(
      function success(odgovor) {
        console.log(odgovor.data.komentarji)
        vm.podatki = { lokacija: odgovor.data };
        vm.glavaStrani = {
          naslov: vm.podatki.lokacija.naziv
        };
      },
      function error(odgovor) {
        console.log(odgovor.e);
      }
      );
    }
    
    vm.prikaziPojavnoOknoObrazca = function() {
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/komentarModalnoOkno/komentarModalnoOkno.pogled.html',
        controller: 'komentarModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              nazivLokacije: vm.podatki.lokacija.naziv,
              avtorMail: vm.avtorMail
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.podatki.lokacija.komentarji.push(podatki);
      }, function() {
        // Ulovi dogodek in ne naredi ničesar
      });
    };
    
    vm.prikaziPojavnoOknoObrazcaUredi = function(vsebina, ocena, idKomentarja, avtor) {
      console.log(vsebina, ocena, idKomentarja)
      var primerekModalnegaOknaUredi = $uibModal.open({
        templateUrl: '/komentarModalnoOknoUredi/komentarModalnoOknoUredi.pogled.html',
        controller: 'komentarModalnoOknoUredi',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              nazivLokacije: vm.podatki.lokacija.naziv,
              vsebina: vsebina,
              ocena: ocena,
              idKomentarja: idKomentarja,
              avtor: avtor
            };
          }
        }
      });
      
      primerekModalnegaOknaUredi.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.posodobiPodatke();
      }, function() {
        // Ulovi dogodek in ne naredi ničesar
      });
      
    };
    
    vm.prikaziPojavnoOknoObrazcaZbrisi = function(idKomentarja) {
      var primerekModalnegaOknaZbrisi = $uibModal.open({
        templateUrl: '/komentarModalnoOknoZbrisi/komentarModalnoOknoZbrisi.pogled.html',
        controller: 'komentarModalnoOknoZbrisi',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              nazivLokacije: vm.podatki.lokacija.naziv,
              idKomentarja: idKomentarja
            };
          }
        }
      });
      
      primerekModalnegaOknaZbrisi.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.posodobiPodatke();
      }, function() {
        // Ulovi dogodek in ne naredi ničesar
      });
      
    };
    
  }
  podrobnostiLokacijeCtrl.$inject = ['$routeParams', '$location', '$uibModal', 'edugeocachePodatki', 'avtentikacija'];
  
  angular
    .module('edugeocache')
    .controller('podrobnostiLokacijeCtrl', podrobnostiLokacijeCtrl);
})();