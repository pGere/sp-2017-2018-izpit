(function() {
  /* global angular */
  
  function komentarModalnoOknoUredi($uibModalInstance, edugeocachePodatki, podrobnostiLokacije) {
    var vm = this;
    
    vm.podrobnostiLokacije = podrobnostiLokacije;

    vm.modalnoOkno = {
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      },
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    
    vm.podatkiObrazca = {
      komentar: vm.podrobnostiLokacije.komentar,
      ocena: vm.podrobnostiLokacije.ocena
    }
    
    vm.posodobiKomentar = function(idLokacije, idKomentarja, podatkiObrazca, avtor) {
      console.log(idLokacije, idKomentarja)
      edugeocachePodatki.posodobiKomentarZaId(idLokacije, idKomentarja, {
        avtor: avtor,
        ocena: podatkiObrazca.ocena,
        besediloKomentarja: podatkiObrazca.komentar
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          console.log(odgovor);
          vm.napakaNaObrazcu = "Napaka pri shranjevanju komentarja, poskusite znova!";
        }
      );
      return false;
    }
    
    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.ocena || !vm.podatkiObrazca.komentar) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        vm.posodobiKomentar(vm.podrobnostiLokacije.idLokacije, vm.podrobnostiLokacije.idKomentarja, vm.podatkiObrazca, vm.podrobnostiLokacije.avtor);
      }
    };
  };
  komentarModalnoOknoUredi.$inject = ['$uibModalInstance', 'edugeocachePodatki', 'podrobnostiLokacije'];
  
  angular
    .module('edugeocache')
    .controller('komentarModalnoOknoUredi', komentarModalnoOknoUredi);
})();