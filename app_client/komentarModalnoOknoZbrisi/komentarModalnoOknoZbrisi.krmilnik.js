(function() {
  /* global angular */
  
  function komentarModalnoOknoZbrisi($uibModalInstance, edugeocachePodatki, podrobnostiLokacije) {
    var vm = this;
    
    vm.podrobnostiLokacije = podrobnostiLokacije;
    
    vm.modalnoOkno = {
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      },
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    
    vm.zbrisiKomentar = function(idLokacije, idKomentarja) {
      edugeocachePodatki.zbrisiKomentarZaId(idLokacije, idKomentarja).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri brisanju komentarja, poskusite znova!";
        }
      );
      return false;
    }
    
    vm.posiljanjePodatkov = function() {
      vm.zbrisiKomentar(vm.podrobnostiLokacije.idLokacije, vm.podrobnostiLokacije.idKomentarja);
    };
  };
  komentarModalnoOknoZbrisi.$inject = ['$uibModalInstance', 'edugeocachePodatki', 'podrobnostiLokacije'];
  
  angular
    .module('edugeocache')
    .controller('komentarModalnoOknoZbrisi', komentarModalnoOknoZbrisi);
})();