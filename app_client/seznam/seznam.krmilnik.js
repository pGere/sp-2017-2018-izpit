(function() {
  /* global angular */
  angular
    .module('edugeocache')
    .controller('seznamCtrl', seznamCtrl);
  
  seznamCtrl.$inject = ['$scope', '$uibModal', 'edugeocachePodatki', 'geolokacija', 'avtentikacija'];
  function seznamCtrl($scope, $uibModal, edugeocachePodatki, geolokacija, avtentikacija) {
    var vm = this;
    vm.glavaStrani = {
      naslov: 'EduGeoCache',
      podnaslov: 'Poiščite zanimive lokacije blizu vas!'
    };
    vm.stranskaOrodnaVrstica = 'Iščete lokacijo za kratkočasenje? EduGeoCache vam pomaga pri iskanju zanimivih lokacij v bližini. Mogoče imate kakšne posebne želje? Naj vam EduGeoCache pomaga pri iskanju bližnjih zanimivih lokacij.';
    vm.sporocilo = "Pridobivam trenutno lokacijo.";
    
    vm.jePrijavljen = avtentikacija.jePrijavljen();
    
    vm.pridobiPodatke = function(lokacija) {
      var lat = lokacija.coords.latitude,
          lng = lokacija.coords.longitude;
      vm.sporocilo = "Iščem bližnje lokacije.";
      edugeocachePodatki.koordinateTrenutneLokacije(lat, lng).then(
        function success(odgovor) {
          vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem lokacij.";
          vm.data = { lokacije: odgovor.data };
        }, 
        function error(odgovor) {
          vm.sporocilo = "Prišlo je do napake!";
          console.log(odgovor.e);
        });
    };
    
    vm.prikaziNapako = function(napaka) {
      $scope.$apply(function() {
        vm.sporocilo = napaka.message;
      });
    };
  
    vm.niLokacije = function() {
      $scope.$apply(function() {
        vm.sporocilo = "Vaš brskalnik ne podpira geolociranja!";
      });
    };
    
    geolokacija.vrniLokacijo(vm.pridobiPodatke, vm.prikaziNapako, vm.niLokacije);
    
    vm.prikaziPojavnoOknoObrazca = function() {
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/lokacijaModalnoOkno/lokacijaModalnoOkno.pogled.html',
        controller: 'lokacijaModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              avtorMail: vm.avtorMail
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.podatki.lokacija.komentarji.push(podatki);
      }, function() {
        // Ulovi dogodek in ne naredi ničesar
      });
    };
  }
})();